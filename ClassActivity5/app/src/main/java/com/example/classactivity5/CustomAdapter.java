package com.example.classactivity5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
    Context context;
    String animalList[];
    int animalImages[];
    LayoutInflater test;

    public CustomAdapter(Context applicationContext, String[] animalList, int[] animalImages )
    {
        this.context = context;
        this.animalList = animalList;
        this.animalImages = animalImages;
        test = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return animalList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = test.inflate(R.layout.rows, parent, false);
        TextView name = (TextView) convertView.findViewById(R.id.textView);
        ImageView countryIcon = (ImageView) convertView.findViewById(R.id.imageView);
        name.setText(animalList[position]);
        countryIcon.setImageResource(animalImages[position]);
        return convertView;

    }


}
