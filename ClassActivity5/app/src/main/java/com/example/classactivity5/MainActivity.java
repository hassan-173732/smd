package com.example.classactivity5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String animalList [] = {"Alex Hales", "Phil Salt", "Asif Ali", "Akif Javed", "Chris Jordan", "R Topley", "Iftikhar Ahmed", "Colin Munro", "Hussain Talat", "Hasan Ali", "Rohail Nazir", "Shadab Khan", "Zafar Gohar", "Faheem Ashraf", "Mohammad Wasim Jr", "Muhammad Musa", "Ahmed Safi Abdullah", "Lewis Gregory"};
        int animalImages [] = {R.drawable.ic_england,R.drawable.ic_england,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_england,R.drawable.ic_england,R.drawable.ic_pakistan,R.drawable.ic_newzeland,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_pakistan,R.drawable.ic_england};

        ListView simpleList = (ListView) findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(),animalList,animalImages);
        simpleList.setAdapter(customAdapter);
    }
}