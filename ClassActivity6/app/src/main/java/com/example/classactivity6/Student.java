package com.example.classactivity6;



public class Student {
    String first;
    String last;
    String age;
    String enrollno;
    String classno;


    public Student(String fname, String lname, String age, String enrollment_number, String classes) {
        this.first = fname;
        this.last = lname;
        this.age = age;
        this.enrollno = enrollment_number;
        this.classno= classes;
    }
}