package com.example.classactivity6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.HashMap;
import java.util.List;

public class CustomAdapter  extends BaseAdapter  {
    Context context;
    HashMap<Integer,Student> studentMap;
    List<Student> studentList;
    LayoutInflater test;

    CustomAdapter (Context context, HashMap<Integer,Student> studentMap) {
        this.context = context;
        this.studentMap = studentMap;
        test = (LayoutInflater.from(context));


    }

    @Override
    public int getCount() {
        return studentMap.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {

        convertView = test.inflate(R.layout.rows, null);
        Student student = studentMap.get(position);
        TextView firstname = (TextView) convertView.findViewById(R.id.fname);
        TextView enrollno = (TextView) convertView.findViewById(R.id.eno);
        TextView lastname = (TextView) convertView.findViewById(R.id.lname);
        TextView classno = convertView.findViewById(R.id.classno);
        TextView age = (TextView) convertView.findViewById(R.id.age);



        firstname.setText(student.first);
        lastname.setText(student.last);
        age.setText(student.age);
        enrollno.setText(student.enrollno);
        classno.setText(student.classno);
        return convertView;
    }

}

