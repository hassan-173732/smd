package com.example.classactivity7;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getSharedPreferences("Prefs", MODE_PRIVATE);
        EditText namebox=findViewById(R.id.editTextTextPersonName2);
        EditText passwordbox=findViewById(R.id.editTextTextPassword2);
        CheckBox remberme = findViewById(R.id.checkBox);
        if(prefs.contains("username"))
        {


            Intent intent = new Intent(this,SecondActivity.class);
            startActivity(intent);

        }
        else
        {
            remberme.setChecked(false);
        }
        Button btn = (Button) findViewById(R.id.button3);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("Prefs", MODE_PRIVATE);
                SharedPreferences.Editor editor=prefs.edit();
                EditText namebox=findViewById(R.id.editTextTextPersonName2);

                EditText passwordbox=findViewById(R.id.editTextTextPassword2);

                CheckBox remberme = findViewById(R.id.checkBox);
                boolean checked = remberme.isChecked();

                if(namebox.getText().toString() != null && passwordbox.getText().toString() != null && checked == true)
                {
                    editor.putString("username", namebox.getText().toString());
                    editor.putString("pass", passwordbox.getText().toString());
                    editor.apply();
                    Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                    startActivity(intent);

                }
                else
                {
                    editor.clear();
                    editor.apply();
                    Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                    startActivity(intent);

                }

            }
        });
    }


}