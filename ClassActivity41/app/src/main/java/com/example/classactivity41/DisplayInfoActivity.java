package com.example.classactivity41;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DisplayInfoActivity extends AppCompatActivity {

    Button button;

    TextView text1;
    TextView text2;
    TextView text3;
    TextView text4;
    TextView text5;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_info);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        text1 = (TextView) findViewById(R.id.textView7);
        text1.setText(extras.getString("Names"));
        text2 = (TextView) findViewById(R.id.textView12);
        text2.setText(extras.getString("CNIC"));
        text3 = (TextView) findViewById(R.id.textView13);
        text3.setText(extras.getString("Email"));
        text4 = (TextView) findViewById(R.id.textView14);
        text4.setText(extras.getString("ADDR"));
        text5 = (TextView) findViewById(R.id.textView16);
        text5.setText(extras.getString("Gender"));






        button = (Button) findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openActivity2();
            }
        });

    }


    private void openActivity2() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}