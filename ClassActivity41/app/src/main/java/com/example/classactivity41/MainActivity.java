package com.example.classactivity41;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Bundle extras = new Bundle();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);

        EditText edit1 = findViewById(R.id.name);
        EditText edit2 = findViewById(R.id.cnic);
        EditText edit3 = findViewById(R.id.email);
        EditText edit4 = findViewById(R.id.addr);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);



        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String name = edit1.getText().toString();
                String cnic = edit2.getText().toString();
                String email = edit3.getText().toString();
                String addr = edit4.getText().toString();
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);

                String gender = radioButton.getText().toString();


                extras.putString("Names",name);
                extras.putString("CNIC",cnic);
                extras.putString("Email",email);
                extras.putString("ADDR",addr);
                extras.putString("Gender",gender);




                openActivity2(extras);
            }
        });
    }
    private void openActivity2(Bundle bundle) {
        Intent intent = new Intent(this, DisplayInfoActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}